package a01a.e1;

import java.util.Iterator;

import java.util.List;
import java.util.function.Predicate;

import a01a.e1.InfiniteSequence;

public class InfiniteSequenceOpsImpl implements InfiniteSequenceOps {

	@Override
	public <X> InfiniteSequence<X> ofValue(X x) {
		
		/*return new InfiniteSequence<X>() {

			@Override
			public X nextElement() {
				
				return x;
			}
		};*/
		
		return () -> x;
	}

	@Override
	public <X> InfiniteSequence<X> ofValues(List<X> l) {
		return new InfiniteSequence<X>() {

			Iterator<X> it = l.iterator();
			
			@Override
			public X nextElement() {
				if(it.hasNext()) {
					return it.next();
				}else {
					it = l.iterator();
					return it.next();
				}
			}
		};
	}

	@Override
	public InfiniteSequence<Double> averageOnInterval(InfiniteSequence<Double> iseq, int intervalSize) {
		
		return () -> iseq.nextListOfElements(intervalSize).stream().mapToDouble(x -> x).average().getAsDouble();
	}

	@Override
	public <X> InfiniteSequence<X> oneEachInterval(InfiniteSequence<X> iseq, int intervalSize) {
		// TODO Auto-generated method stub
		return () -> iseq.nextListOfElements(intervalSize).stream().skip(intervalSize - 1).findFirst().get();
	}

	@Override
	public <X> InfiniteSequence<Boolean> equalsTwoByTwo(InfiniteSequence<X> iseq) {
		return () -> {
			
			return iseq.nextElement().equals(iseq.nextElement());
		};
	}

	@Override
	public <X, Y extends X> InfiniteSequence<Boolean> equalsOnEachElement(InfiniteSequence<X> isx,
			InfiniteSequence<Y> isy) {
		// TODO Auto-generated method stub
		return () -> isx.nextElement().equals(isy.nextElement());
	}

	@Override
	public <X> Iterator<X> toIterator(InfiniteSequence<X> iseq) {
		// TODO Auto-generated method stub
		return new Iterator<X>() {

			@Override
			public boolean hasNext() {
				return true;
			}

			@Override
			public X next() {
				return iseq.nextElement();
			}
		};
	}

}
